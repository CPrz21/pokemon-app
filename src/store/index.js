import Vue from 'vue';
import Vuex from 'vuex';
import { addToFavorites, removeToFavorites } from '../../utils/functions';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    favorites: []
  },
  mutations: {
    addFavorite(state, { pokemon, vm }) {
      state.favorites = [...state.favorites, pokemon];
      addToFavorites(pokemon, vm);
    },
    removeFavorite(state, { pokemon, vm }) {
      removeToFavorites(pokemon, vm);
      let pokemons = JSON.parse(localStorage.getItem('favorites'));
      state.favorites = pokemons;
    },
    getFavoritesPokemon(state, payload) {
      state.favorites = payload;
    }
  },
  actions: {},
  modules: {}
});
