import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import '@/assets/css/tailwind.css';
import EvaIcons from 'vue-eva-icons';
import VueToastify from 'vue-toastify';
import VueLocalStorage from 'vue-localstorage';
import VModal from 'vue-js-modal';

Vue.config.productionTip = false;

Vue.use(EvaIcons);
Vue.use(VueToastify);
Vue.use(VueLocalStorage);
Vue.use(VModal, { dialog: true });

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
