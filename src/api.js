import axios from 'axios';
import { get } from 'lodash';

import { apiUrl } from '../utils/constants';

async function getPokemonsRequest(url) {
  if (url === null) {
    url = `${apiUrl}/pokemon?limit=40`;
  }

  let data = await axios.get(`${url}`);
  return get(data, 'data', null);
}

async function getPokemonDetailRequest(pokemonId) {
  let data = await axios.get(`${apiUrl}/pokemon/${pokemonId}`);
  return get(data, 'data', null);
}

export default {
  getPokemonsRequest,
  getPokemonDetailRequest
};
