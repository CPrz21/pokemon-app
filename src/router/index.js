import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import About from '../views/About.vue';
import Favorites from '../views/Favorites.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/pokemon',
    name: 'Home',
    component: Home
  },
  {
    path: '/pokemon/favorites',
    name: 'Favorites',
    component: Favorites
  },
  {
    path: '/pokemon/:id',
    name: 'About pokemon',
    component: About
  },
  { path: '*', redirect: '/pokemon' }
];

const router = new VueRouter({
  routes
});

export default router;
