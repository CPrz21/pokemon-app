import { find, findIndex } from 'lodash';

const successMessage = vm => {
  vm.$vToastify.success({
    title: 'Added!',
    body: 'You have a new pokemon',
    type: 'success',
    canTimeout: true,
    defaultTitle: true,
    duration: 2000
  });
};

const errorMessage = vm => {
  vm.$vToastify.success({
    title: 'Sorry!',
    body: 'You already have this pokemon',
    type: 'info',
    canTimeout: true,
    defaultTitle: true,
    duration: 2000
  });
};

const removeMessage = vm => {
  vm.$vToastify.success({
    title: 'Removed!',
    body: 'Pokemon removed',
    type: 'info',
    canTimeout: true,
    defaultTitle: true,
    duration: 2000
  });
};

export const addToFavorites = (pokemon, vm) => {
  if (findPokemonInFavorites(pokemon, vm)) {
    return errorMessage(vm);
  }
  if (localStorage.getItem('favorites')) {
    const favorites = JSON.parse(vm.$localStorage.get('favorites'));
    const parsed = JSON.stringify([...favorites, pokemon]);

    vm.$localStorage.set('favorites', parsed);
  } else {
    const parsed = JSON.stringify([pokemon]);

    vm.$localStorage.set('favorites', parsed);
  }
  return successMessage(vm);
};

export const removeToFavorites = (pokemon, vm) => {
  let favorites = JSON.parse(vm.$localStorage.get('favorites'));
  let pokemonIndex = findIndex(favorites, ['name', pokemon.name]);
  favorites.splice(pokemonIndex, 1);
  const parsed = JSON.stringify(favorites);
  vm.$localStorage.set('favorites', parsed);
  removeMessage(vm);
};

export const handleSuccessMessage = vm => {
  successMessage(vm);
};

export const handleErrorMessage = vm => {
  errorMessage(vm);
};

const findPokemonInFavorites = (pokemon, vm) => {
  const favorites = JSON.parse(vm.$localStorage.get('favorites'));
  let pokemonExist = find(favorites, ['name', pokemon.name]);
  return pokemonExist;
};
