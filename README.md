# Vue Pokemon App

![vue](https://hackernoon.com/hn-images/1*ACR0gj0wbx91V_xgURifWg.png)
![pokemon](https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/International_Pok%C3%A9mon_logo.svg/1280px-International_Pok%C3%A9mon_logo.svg.png)

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Node version

```
12.16.2
```

### Yarn version

```
1.22.4
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
